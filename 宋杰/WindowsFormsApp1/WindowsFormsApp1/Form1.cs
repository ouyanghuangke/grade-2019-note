﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“schoolsDataSet3.students”中。您可以根据需要移动或删除它。
            //this.studentsTableAdapter3.Fill(this.schoolsDataSet3.students);

            var sql = "select * from Students";

            var dt = DBhelp.GetDataTable(sql);

            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stu = "select * from students";

                var dt = DBhelp.GetDataTable(stu);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where StudentsName like '%{0}%'",name);

            var dt = DBhelp.GetDataTable(sql);

            dataGridView1.DataSource = dt;

        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button3_Click_1(object sender, EventArgs e)
        {

            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["Studentsname"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            Form2 form = new Form2(id, name, age, score);
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var com = "select * from students";

                var dt = DBhelp.GetDataTable(com);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {

                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from students where Id={0}", id);
                var res = DBhelp.AddOrUpdate(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var stu = "select * from Students";
                    var dt = DBhelp.GetDataTable(stu);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择要删除的数据", "提示");
            }
        }
    }
}
