<?php
declare (strict_types=1);

namespace app\index\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\View;

class Index
{
    public function index()
    {
        $articleList = ArticleModel::order("add_time desc")->paginate(10);
        return View::fetch('', [
            'articleList' => $articleList,
        ]);
    }
}
