<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/5/18
 * Time: 14:26
 */

namespace app\model;


use think\Model;

class CommentModel extends Model
{
    protected $name='comment';
    protected $pk='comment_id';

}