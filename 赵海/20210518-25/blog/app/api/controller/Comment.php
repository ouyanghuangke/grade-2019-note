<?php
declare (strict_types=1);

namespace app\api\controller;


use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;

class Comment
{
    //增加评论
    public function add(){
        $params=Request::param();
        $validate=Validate::rule([
            'comment_content'=>'require|min:5|max:255',
            'email'=>'require|min:3|max:20',
            'nickname'=>'require|min:2|max:25',
            'article_id'=>'require|between:1,' . PHP_INT_MAX,
        ]);

        if (!$validate->check($params)){
            $data=[
                'status'=>3,
                'message'=>$validate->getError(),
                'data'=>[]
            ];
            return json($data);
        }

        //验证内容是否违规
        require_once '../extend/baidu/AipContentCensor.php';
        $client = new \AipContentCensor(24246085, "D7MiB5rvplROHQGIfCDGjTPg", "THUhSMVFZzranN7tKnLWEYAzeX42NtL3");
        $result = $client->textCensorUserDefined($params['comment_content'] . ' ' , $params['email'] . ' ' ,$params['nickname']);
        if ($result['conclusionType'] == 2){
            $data=[
              'status'=>3,
                'message'=>'评论内容涉及敏感词',
                'data'=>[],
            ];
            return json($data);
        }
        if ($result['conclusionType'] == 3){
            //疑似
            $insertData=$params;
            $insertData['add_time']=$insertData['update_time']=time();
            $insertData['status']=2;
            CommentModel::create($insertData);
            $data=[
                'status'=>3,
                'message'=>'评论内容涉及敏感词',
                'data'=>[],
            ];
            return json($data);
        }

        $insertData=$params;
        $insertData['add_time']=$insertData['update_time']=time();
        $result=CommentModel::create($insertData);
        if ($result){
            $data=[
                'status'=>0,
                'message'=>'',
                'data'=>[
                    'comment'=>$result->toArray()
                ]
            ];
            return json($data);
        }
        $data=[
            'status'=>1,
            'message'=>'显示失败',
            'data'=>[
                'comment'=>[]
            ]
        ];
        return json($data);

    }
    public function list()
    {
        $articleId=Request::param('article_id');
//        var_dump($articleId);
//        exit();
        $validate=Validate::rule([
            'article_id'=>'require'
        ]);
        if (!$validate->check(['article_id'=>$articleId])){
            $data=[
                'status'=>3,
                'message'=>$validate->getError(),
                'data'=>[],
            ];
            return json($data);
        }

        $result=CommentModel::where("article_id","=",$articleId)
            ->order('add_time','desc')
            ->select();

        $commentList=[];
        foreach ($result as $item){
            $temp=$item->toArray();
            $commentList[]=[
                'comment_id'=>$temp['comment_id'],
                'comment_content'=>$temp['comment_content'],
                'nickname'=>$temp['nickname'],
                'add_time'=>$temp['add_time'],
            ];

        }
        $data=[
            'status'=>0,
            'message'=>'',
            'data'=>[
                'commentList'=>$commentList
            ],
        ];
        return json($data);

    }
}
