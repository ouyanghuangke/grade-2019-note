<?php
declare (strict_types=1); // php的严格模式，弱类型

namespace app\admin\controller;

use app\model\ArticleModel;
use app\model\CommentModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Comment
{
    // 列表
    public function index()
    {
        $status=Request::param('status',1);
        // 查找列表数据
        $commentList = CommentModel ::where('status','=',$status)->select();

        View::assign('commentList', $commentList);
        View::assign('status', $status);

        return View::fetch();
    }

    //回收站移回正常
    public function recover(){

        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $comment=CommentModel::find($commentId);
        $comment['status'] = 1;
        $result=$comment->save();

        return View::fetch('public/tips',['result'=>$result]);

    }



    // 正常移入回收站
    public function rubbish()
    {
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $comment=CommentModel::find($commentId);
        $comment['status'] = 2;
        $result=$comment->save();

        $data=[
            'status'=>$result ? 0 : 23423,
            'message'=> $result ? '' : '修改数据库失败',
            'data'=>[
                'result'=>$result ? true : false
            ]
        ];

        return json($data);
    }

    //删除
    public function delete()
    {
        $commentId = Request::param("comment_id");
        $validate = Validate::rule([
            'comment_id|评论id' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        if (!$validate->check(['comment_id' => $commentId])) {
            echo $validate->getError();
            exit();
        }

        $result=CommentModel::destroy($commentId);

        $data=[
            'status'=>$result ? 0 : 23423,
            'message'=> $result ? '' : '修改数据库失败',
            'data'=>[
                'result'=>$result ? true : false
            ]
        ];
        return json($data);
    }
}
