<?php
session_start();
$Code=$_POST['code'];

$adminEmail=$_POST['admin_email'];
$adminPassword=$_POST['admin_password'];
$remberMe=$_POST['rember_me'] ?? '';
$code = $_SESSION['code'];

date_default_timezone_set("PRC");

if(strcasecmp($Code,$code)){
    if($Code!=$code){
        echo "验证码不正确";
        echo "<a href='index.php?c=login'>返回登录页面</a>";
        $log = [
            'code'=>$Code,
            'content' => '登录后台失败',
            'time' => date("Y-m-d H:i:s", time())
        ];
        $file=fopen("log.text","a+");
        fwrite($file,json_encode($log, JSON_UNESCAPED_UNICODE).PHP_EOL);
        exit();
    }
}
include_once APP_PATH . "./model/login_save.php";
include_once APP_PATH . "./view/login_save.php";
