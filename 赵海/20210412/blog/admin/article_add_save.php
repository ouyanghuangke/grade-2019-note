<?php
$articleTitle=$_POST['article_title'];
$categoryId=$_POST['category_id'];
$intro=$_POST['intro'];
$content=$_POST['content'];

if(mb_strlen($articleTitle)<5 || mb_strlen($articleTitle)>50){
    echo "文章标题限制5-50";
    echo'<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    exit();
}

if(mb_strlen($intro)<10 || mb_strlen($intro)>500){
    echo "文章标题限制10-500";
    echo'<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    exit();
}
if(mb_strlen($content)<10 || mb_strlen($content)>1000){
    echo "文章内容限制10-1000";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}
if(empty($categoryId)){
    echo "需要选择文章分类";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}


date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$addTime=time();
$updateTime=$addTime;



$sql="insert into article (article_title,category_id,intro,content,add_time,update_time)
      values ('$articleTitle','$categoryId','$intro','$content','$addTime','$updateTime')";
$result=$db->exec($sql);


if($result){
    echo "插入成功<a href='article_list.php'>返回列表页面</a>";

    $log = [
        'article_title' => $articleTitle,
        'category_id' => $categoryId,
        'intro'=>$intro,
        'content'=>$content,
        'content' => '增加分类成功',
        'time' => date("Y-m-d H:i:s", time())
    ];
    $file=fopen("log.text","a+");
    fwrite($file,json_encode($log,JSON_UNESCAPED_UNICODE).PHP_EOL);
    exit();
}else{
    echo "插入数据失败，错误信息<a href='article_add.php'>添加页面</a>";
}

