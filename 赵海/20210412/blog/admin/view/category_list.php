<?php
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link href="css/index.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery.js"></script>
</head>

<body>
<div id="top" style="width:1440px;">
    <div id="ttop" style="width:300px;float:left;"><h2>博客管理系统</h2></div>
    <div id="rtop" style="width:1140px;float:left;"><h5>欢迎你:<?php echo $_SESSION['admin_name']?>
            <a href="index.php?c=logout">退出登录</a></h5></div>
</div>
<div id="button">
    <div id="bleft">
        <ul>
            <li>
                <a href="index.php?c=category_list">分类管理</a>
            </li>
            <li>
                <a href="index.php?c=article_list">文章管理</a>
            </li>
            <li>
                <a href="index.php?c=index.php?c=admin_list">管理员</a>
            </li>
        </ul>
    </div>
    <form action="index.php?c=category_delete_qx.php"  method="get" id="plsc" >
        <div id="bright">
            <a href="#">首页</a>&nbsp;&nbsp;&nbsp;><a href="#">分类管理</a>&nbsp;&nbsp;&nbsp;><a href="#">分类列表</a>
            <input type="button" id="b" value="全选" />
            <a  class="plsc" href="javascript:void(0); ">删除选中任务</a>
            <a href="index.php?c=category_add" class="zj">增加分类</a>
            <table border="1" align="center" cellspacing="0" width="1110px;" >
                <tr>
                    <th> </th>
                    <th>分类ID</th>
                    <th>分类名称</th>
                    <!--                    <th>分类</th>-->
                    <!--                    <th>简介</th>-->
                    <th>增加时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($categoryList as $row): ?>
                    <tr align="center">
                        <td>
                            <input name="checked[]" value="<?php echo $row['category_id'];?>" style="width:25px; vertical-align:middle; margin-top:0px;" type="checkbox" class="checkbox" />
                        </td>
                        <td><?php echo $row['category_id'];?></td>
                        <td><?php echo $row['category_name'];?></td>
                        <!--                    <td>web前端</td>-->
                        <!--                    <td>vue入门指南，适合刚接触vue的用户</td>-->
                        <td><?php echo date("Y-m-d H:i:s", $row['add_time']);?></td>
                        <td><?php echo date("Y-m-d H:i:s", $row['update_time']);?></td>
                        <td width="70px;"><a href="index.php?c=category_edit&category_id=<?php echo $row['category_id']?>">编辑</a>
                            <a href="index.php?c=category_delete&category_id=<?php echo $row['category_id']?>">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </form>
</div>




<script src="js/main.js"></script>
</body>
</html>

