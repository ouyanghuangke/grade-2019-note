<?php
//在Human类里声明 姓名、性别、生日
//class Human{
//    public $name;
//    public $sex;
//    public $birthday;
//
//    public function eat(){
//        echo "干饭";
//    }
//    public function sleep(){
//        echo "睡觉";
//    }
//    public function play(){
//        echo "玩";
//    }
//}


//$Human1=new Human();
//$Human1->name='小明';
//$Human1->sex='男';
//$Human1->birthday='2020-10-05';
//var_dump($Human1);
//$Human1->sleep();
//
//
//$human2 = new Human();
//$human2->name = "小红";
//$human2->sex = "女";
//$human2->birthday = "2002-02-16";

//echo $Human1->sex;

//class Car{
//    public $brand;
//    public $arctic;
//    public $price;
//}
//$car=new Car();
//$car->brand='雷克萨斯';
//$car->arctic='LC 2020款';
//$car->price='120W';
//var_dump($car);
//
//class Human
//{
//    public $name;
//    public $sex;
//    public $birthday;
//    public function __construct($name,$sex,$birthday)
//    {
//        $this->name=$name;
//        $this->sex=$sex;
//        $this->birthday=$birthday;
//    }
//}
//$human1=new Human("小军","男","2001-08-26");
//var_dump($human1);





class Human
{
    public $name; // 姓名
    public $sex; // 性别
    public $birthday; // 出生年月
}

class Student extends Human
{
    public $studentId; // 学号
    public $university; // 所在大学
    public $institute; // 所在院系
    public $class; // 所在班级
}

$student=new Student();
$student->name='小明';
$student->sex='男';
$student->birthday='2020-10-05';
$student->studentId='2020202';
$student->university='中国';
$student->class='5班';

class Worker extends Human
{
    public $workerId; // 工号
    public $salary; // 薪资
}