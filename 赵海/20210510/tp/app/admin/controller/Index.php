<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\Request;
use think\facade\Validate;

class Index
{
    public function index()
    {
        return '您好！这是一个[admin]示例应用';
    }

    //        接第一章节练习题第2题，现在需要改进接口，移除首尾的空格再计算长度，请使用版本设计的方式改进。
    public function zfc()
    {
        $v = Request::param('v', 1);
        if ($v == 1) {
            $str = Request::param('str');
            $validate = Validate::rule([
                'str' => 'require',
            ]);
            if (!$validate->check(['str' => $str])) {
                $data = [
                    'status' => 1,
                    'message' => $validate->getError(),
                    'data' => []
                ];
                return json($data);
            }
            $b = trim($str);
            $zfc = mb_strlen($b);
            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'length' => $zfc,
                ]
            ];
            return json($data);
        } else if ($v == 2) {
            return "v2";
        }
    }
        public function word(){
        $content=Request::param('content');
        $validate=Validate::rule([
            'content'=>'require|min:5|max:100',
        ]);
        if (!$validate->check(['content'=>$content])){
            echo "你猜啊";
            exit();
        }
        $words=explode("",$content);
        $date=[
            'status' => 0,
            'message' => '',
            'data' =>$words,
        ];
        return json($date);
    }

}
