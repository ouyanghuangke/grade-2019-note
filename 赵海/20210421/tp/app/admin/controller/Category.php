<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\CategoryModel;
use app\model\dsModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Category
{
    public function index()
    {
        //作业查询4
//        $result=dsModel::select();
//        foreach ($result as $item){
//            echo "id" . $item['id'];
//            echo "<br />";
//            echo "名称" . $item['name'];
//            echo "<br />";
//            echo  "分类id" . $item['cate_id'];
//            echo "<br />";
//            echo "品牌" .$item['brand_id'];
//            echo "<br />";
//            echo "价格" .$item['price'];
//            echo "<br />";
//            echo $item['is_show'];
//            echo "<br />";
//            echo $item['is_saleoff'];
//            echo "<br />";
//        }

        //作业5  查询所有的商品，并且按照价格降序排列。

//        $result=dsModel::order('price','desc')->select();
//        foreach ($result as $item){
//            echo "id:" . $item['id'];
//            echo "<br />";
//            echo "名称:" . $item['name'];
//            echo "<br />";
//            echo  "分类:" . $item['cate_id'];
//            echo "<br />";
//            echo "品牌:" .$item['brand_id'];
//            echo "<br />";
//            echo "价格:" .$item['price'];
//            echo "<br />";
//            echo $item['is_show'];
//            echo "<br />";
//            echo $item['is_saleoff'];
//            echo "<br />";
//            echo "<br />";
//        }
        //作业6 查询出品牌是戴尔的商品。
//        $result=dsModel::where('brand_id',4)->find();
//        echo $result['name'];

        //作业7  查询出所有超极本的商品.
//        $result=dsModel::where('cate_id',5)->select();
//        foreach ($result as $item){
//            echo $item['name'];
//            echo "<br />";
//        }
        //作业8. 查询华硕品牌的超极本。
        $result=dsModel::where('cate_id',5)
            ->where('',)
            ->select();
        foreach ($result as $item){
            echo $item['name'];
            echo "<br />";
        }


        //查询出所有分类数据
//        $result=CategoryModel::select([13,14]);
//        foreach ($result as $item){
//            echo $item['category_id'];
//            echo "<br />";
//            echo $item['category_name'];
//            echo "<br />";
//            echo $item['category_desc'];
//            echo "<br />";
//            echo $item['update_time'];
//            echo "<br />";
//            echo $item['add_time'];
//        }
//        return View::fetch();
        //查询id为13的分类，并打印
//        $result=CategoryModel::where('category_id',13)->find();
//        echo $result['category_name'];

        //筛选所有分类，按照category_id降序排列
        //查询3条
        //筛选id小于10
//        $result=CategoryModel::where('category_id','>',10)
//            ->order('category_id','desc')
//            ->limit(0,3)
//            ->select();
//        foreach ($result as $item){
//            echo $item['category_id'];
//            echo "<br />";
//            echo $item['category_name'];
//            echo "<br />";
//            echo $item['category_desc'];
//            echo "<br />";
//            echo $item['update_time'];
//            echo "<br />";
//            echo $item['add_time'];
//            echo "<br />";
//        }

        //查询分类名称为mysql的分类，并打印
//        $result=CategoryModel::where('category_name','mysql')->find();
//        echo '分类名称:' . $result['category_name'];
//        echo '分类描述:' . $result['category_desc'];

        //查询分类id为13的分类
//        $result=CategoryModel::getByCategoryId('13');
//        echo '分类名称:' . $result['category_name'];
//        echo '分类描述:' . $result['category_desc'];
    }
    public function add(){
        //分类增加法模板渲染
        return View::fetch();
    }
    public function addSave(){
        $category=CategoryModel::create([
            'category_name'=>'thinkphp',
            'category_desc'=>'thinkphp学习',
            'update_time'=>time(),
            'add_time'=>time(),
        ]);
//        var_dump($category);
        echo $category->category_id;
        echo $category->category_name;
        echo $category->category_desc;
        echo $category->update_time;
        echo $category->add_time;

    }
    public function edit(){
        return View::fetch('add');
    }
    public function editSave(){
        //修改
//        $result=CategoryModel::find(13);
//        $result['category_desc']='abcdefg';
//        $result->save();
//
        //作业2.3
//        $result=dsModel::find(2);
//        $result['is_show']='0';
//        $result->save();

    }
    public function head(){
        return View::fetch();
    }
    public function delete(){
        $categoryId=Request::param('category_id');
        $data=[
            'category_id'=>$categoryId,
            'category_name'=>'php'
        ];

        $validate=Validate::rule([
            'category_id|分类id'=>'require|between:1,' . PHP_INT_MAX, // require必须，正整数
            'category_name|分类名称'=>'require|min:2|max:45',
        ]);
        //检测数据
        $checkResult=$validate->check($data);
        //验证检测结果
        if($checkResult){
            echo "校验通过";
        }else{
            echo $validate->getError();
        }
        return '';
    }
}
//1. 参考 82_电商数据.md，系统增加了品牌、分类和商品，将其中相关数据插入到对应表。
//	2. r510vc 15.6英寸笔记本 已经售罄，请修改相关数据。
//	3. 15.6 寸电脑屏保护膜 下架处理，请修改相关数据。
//	4. 查询所有的商品、品牌和分类数据。

//	5. 查询所有的商品，并且按照价格降序排列。
//	6. 查询出品牌是戴尔的商品。

//	7. 查询出所有超极本的商品。
//	8. 查询华硕品牌的超极本。

//	9. 查询出价格最高的商品和最低的商品。
//	10. 查询是否有售罄的商品。
//	11. 查询所有商品、品牌和分类总数。
//	12. 每页显示10件商品，查询出第1页和第2页的数据。