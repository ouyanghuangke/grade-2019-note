<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\CategoryModel;
use think\facade\View;

class Category
{
    public function index()
    {
        $categoryList=CategoryModel::select();

        View::assign('categoryList',$categoryList);
        return View::fetch();
    }
}
