<?php
date_default_timezone_set("PRC");
define("APP_PATH",dirname(__FILE__));

$controller=$_GET['c'] ?? "";

if(empty($controller)){
    echo "页面没有找到";
    exit();
}

include_once "./controller/{$controller}.php";

