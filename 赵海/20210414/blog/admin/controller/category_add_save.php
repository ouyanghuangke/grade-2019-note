<?php
$categoryId=$_POST['category_id'];
$categoryName=$_POST['category_name'];
$category_Desc=$_POST['category_desc'];

if(empty($categoryId)){
    echo "文章分类不能为空";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}

if(mb_strlen($categoryName)<5 || mb_strlen($categoryName) >50){
    echo "文章名称限制5-50";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}
if(mb_strlen($category_Desc)<10 || mb_strlen($category_Desc) >200){
    echo "分类描述10-200";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}


include_once APP_PATH . "./model/category_add_save.php";
include_once APP_PATH . "./view/category_add_save.php";

