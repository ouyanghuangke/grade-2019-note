/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-04-01 10:23:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(10) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(30) NOT NULL,
  `admin_password` varchar(25) NOT NULL,
  `admin_email` varchar(25) NOT NULL,
  `add_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'sa', '123', '1234@qq.com', '1617239713', '1617239714');
INSERT INTO `admin` VALUES ('2', 'ax', '123456', '123456@qq.com', '1617239714', '1617239714');
INSERT INTO `admin` VALUES ('3', 'jj', '1234', '12345@qq.com', '1617241911', '1617241911');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
  `intro` varchar(500) NOT NULL COMMENT '简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('7', '13', 'fsadfsa', ' asdghshd', 'hgfkhg', '1616639008', '1616639008');
INSERT INTO `article` VALUES ('12', '13', 'wfadgfa', ' ahdshggfshfdjfdhjdgjedjk', ' ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk ahdshggfshfdjfdhjdgjedjk', '1616640489', '1616640489');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('13', 'gfdasfgsdag', 'sdagfsad', '1616638998', '1616638998');
