<?php
session_start();
if(empty($_SESSION['admin_email'])){
    echo "请先登录<a href='login.php'>登录页面</a>";
    echo exit();
}

date_default_timezone_set("PRC");

$dsn="mysql:host=127.0.0.1;dbname=blog;";
$db =new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");

$sql="select * from article order by article_id desc";
$result=$db->query($sql);
$articleList=$result->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/index.css" rel="stylesheet" type="text/css"/>
</head>

<body>
	<form>
	<div id="top" style="width:1440px;">
        <div id="ttop" style="width:300px;float:left;"><h2>博客管理系统</h2></div>
        <div id="rtop" style="width:1140px;float:left;"><h5>欢迎你:<?php echo $_SESSION['admin_name']?>
                <a href="logout.php">退出登录</a></h5></div>
    </div>
    <div id="button">
    <div id="bleft">
    	<ul>
        <li>
                    <a href="category_list.php">分类管理</a>
                </li>
                <li>
                    <a href="article_list.php">新闻管理</a>
                </li>
                <li>
                    <a href="admin_list.php">管理员</a>
                </li>
        </ul>
    </div>
    <div id="bright">
    	<a href="#">首页</a>&nbsp;&nbsp;&nbsp;><a href="#">新闻管理</a>&nbsp;&nbsp;&nbsp;><a href="#">新闻列表</a>
        <input type="button" id="b" value="全选" />
        <a href="#">删除选中任务</a>
        <a href="article_add.php" class="zj">增加新闻</a>
        <table border="1" align="center" cellspacing="0" width="1110px;" >
       <tr>
       		<th> </th>
        	<th>ID</th>
            <th>标题</th>
            <th>分类名称</th>
            <th>简介</th>
            <th>发表时间</th>
            <th>修改时间</th>
            <th>操作</th>
        </tr>
            <?php foreach ($articleList as $item): ?>
        <tr align="center">
        	<td>
            	<input style="width:25px; vertical-align:middle; margin-top:0px;" type="checkbox" class="checkbox" />
            </td>
            <td><?php echo $item['article_id']?></td>
            <td><?php echo $item['article_title']?></td>
            <td><?php
//                echo $item['category_id'];
                $sql="select * from category where category_id='{$item['category_id']}'";
                $result=$db->query($sql);
                $category=$result->fetch(PDO::FETCH_ASSOC);
                echo $category['category_name'];
                ?></td>
            <td><?php echo $item['intro']?></td>
            <td><?php echo date("Y-m-d H:i:s", $item['add_time'])?></td>
            <td><?php echo date("Y-m-d H:i:s", $item['update_time'])?></td>
            <td width="70px;"><a href="article_edit.php?article_id=<?php echo $item['article_id']?>">编辑</a>
                <a href="article_delete.php?article_id=<?php echo $item['article_id']?>">删除</a>
            </td>
        </tr>
            <?php endforeach; ?>
         </table>
        </div>

    </div>


    </form>
</body>
</html>
