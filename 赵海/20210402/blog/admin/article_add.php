<?php
date_default_timezone_set("PRC");
$dsn="mysql:host=127.0.0.1;dbname=blog";
$db=new PDO($dsn,"root","123456");
$db->exec("set names utf8mb4");
$sql="select * from category order by category_id desc";
$result=$db->query($sql);
$categoryList=$result->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" href="css/index2.css" type="text/css" />
</head>

<body>
<form action="article_add_save.php" method="post">
    <div id="top" style="width:1440px;">
        <div id="ttop" style="width:300px;float:left;"><h2>博客管理系统</h2></div>
        <div id="rtop" style="width:1140px;float:left;"><h5>欢迎你:admin退出登录</h5></div>
    </div>
    <div id="button">
        <div id="bleft">
            <ul>
                <li>
                    <a href="article_list.php" class="fenlei">分类管理</a>
                </li>
                <li>
                    <a href="category_list.php" class="xinwen">新闻管理</a>
                </li>
                <li>
                    <a href="admin_list.php" class="guanli">管理员</a>
                </li>
            </ul>
        </div>
        <div id="bright">
            <a href="#">首页</a>&nbsp;&nbsp;&nbsp;><a href="#">文章管理</a>&nbsp;&nbsp;&nbsp;><a href="#">增加文章</a>

            <table border="1" cellspacing="0" width="1000px;" >
                <!--                <tr>-->
                <!--                    <td>新闻id:</td>-->
                <!--                    <td><input type="text"/></td>-->
                <!--                </tr>-->
                <tr>
                    <td>文章标题:</td>
                    <td><input type="text" name="article_title" /></td>
                </tr>
                                <tr>
                                    <td>所属分类:</td>
                                    <td>
                                        <select name="category_id">
                                            <option value="0">请选择分类</option>
                                            <?php foreach ($categoryList as $row): ?>
                                            <option
                                                    value="<?php echo $row['category_id']?>">
                                                <?php echo $row['category_name']?>
                                            </option>
                                            <?php endforeach; ?>

                                        </select>
                                    </td>
                                </tr>
                <tr>
                    <td>文章简介:</td>
                    <td><textarea rows="10" cols="30" name="intro"> </textarea></td>
                </tr>
                <tr>
                    <td>文章内容:</td>
                    <td><textarea rows="10" cols="30" name="content"></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="提交" />
                        <input type="reset" value="重置" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
</form>
</body>
</html>
