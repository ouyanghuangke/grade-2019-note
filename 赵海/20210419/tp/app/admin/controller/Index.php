<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\View;

class Index
{
    public function index()
    {
        //view是一个视图对象
        $count=10;
        $arr=[1,2,3,4,5];
//        View::assign('count',$count);
//        View::assign('arr',$arr);
        return View::fetch('',['count'=>$count,'arr'=>$arr]);
//        return '您好！这是一个[admin]示例应用';
    }
}
