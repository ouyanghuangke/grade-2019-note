<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Category
{
    public function index()
    {
        return View::fetch();
//        return '您好！这是一个[admin]示例应用';
    }
    public function add(){
        //分类增加法模板渲染
        return View::fetch();
    }
    public function edit(){
        return View::fetch();
    }
    public function head(){
        return View::fetch();
    }
    public function delete(){
        $categoryId=Request::param('category_id');
        $data=[
            'category_id'=>$categoryId,
            'category_name'=>'php'
        ];

        $validate=Validate::rule([
            'category_id|分类id'=>'require|between:1,' . PHP_INT_MAX, // require必须，正整数
            'category_name|分类名称'=>'require|min:2|max:45',
        ]);
        //检测数据
        $checkResult=$validate->check($data);
        //验证检测结果
        if($checkResult){
            echo "校验通过";
        }else{
            echo $validate->getError();
        }
        return '';
    }
}
