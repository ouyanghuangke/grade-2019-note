<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/21
 * Time: 17:25
 */

namespace app\model;


use think\Model;

class CategoryModel extends Model
{
    protected $name='category';//表名
    protected $pk='category_id';//主键 id
}