<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/23
 * Time: 10:21
 */

namespace app\model;


use think\Model;

class ArticleModel extends Model
{
    protected $name='article';
    protected $pk='article_id';

}