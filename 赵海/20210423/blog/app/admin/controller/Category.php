<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Category
{
    public function index()
    {
        $categoryList=CategoryModel::select();

        View::assign('categoryList',$categoryList);
        return View::fetch();
    }
    public function add(){

        return View::fetch();

    }
    public function addSave(){
        $params=Request::param();
        $validate=Validate::rule([
            'category_name|分类名称'=>'require|min:2|max:45',
            'category_desc|分类描述'=>'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            echo $validate->getError();
            exit();
        }
        $params['add_time']=time();
        $params['update_time']=time();
        $result=CategoryModel::create($params);

        return View::fetch('public/tips',['result'=>$result]);
    }
    public function edit(){
        $categoryId=Request::param(['category_id']);
        $validate=Validate::rule([
            'category_id'=>'require',
        ]);
        if (!$validate->check(['category_id'=>$categoryId])) {
            echo $validate->getError();
            exit();
        }
        $category=CategoryModel::find($categoryId);
        return View::fetch('',['category'=>$category]);

    }
    public function editSave(){
        $params=Request::param();
        $validate=Validate::rule([
            'category_id'=>'require',
            'category_name'=>'require|min:2|max:45',
            'category_desc'=>'require|min:10|max:255'
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        $category=CategoryModel::find($params['category_id']);
        if(!$category){
            echo '分类存在';
            exit();
        }
        $category['category_name']=$params['category_name'];
        $category['category_desc']=$params['category_desc'];
        $result=$category->save();
        return View::fetch('public/tips',['result'=>$result]);
    }
    public function delete(){
        $categoryId=Request::param('category_id');
        $validate=Validate::rule([
            'category_id'=>'require',
        ]);
        if(!$validate->check(['category_id'=>$categoryId])){
            echo $validate->getError();
            exit();
        }
        $article=ArticleModel::where('category_id','=',$categoryId)->find(1);
        if($article){
            echo "分类下有文章,请先删除相关的文章";
            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
            exit();
        }

        $result=CategoryModel::destroy($categoryId);
        return View::fetch('public/tips',['result'=>$result]);
    }
}

