<?php

declare (strict_types = 1);
namespace app\admin\controller;


use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Article
{
    public function index(){
        $articleList=ArticleModel::select();

        foreach ($articleList as $row){
            $category=CategoryModel::find($row['category_id']);
            $row['category_name']=$category['category_name'];
        }
        unset($row);
        View::assign('articleList',$articleList);
        return View::fetch();
    }
    public function add(){
        return View::fetch();
    }
    public function addSave(){   //有出错
        $param=Request::param();
        $validate=Validate::rule([
            'article_title|文章标题'=>'require|min:2|max:45',
            'intro|文章简介'=>'require|min:5|max:255',
            'content|文章内容'=>'require|min:5|max:255',
            'category_id'=>'require|between:1' . PHP_INT_MAX
        ]);
        if($validate->check($param)){
            echo $validate->getError();
            echo exit();
        }
        $param['add_time']=time();
        $param['update_time']=time();
        $result=ArticleModel::create($param);

        return View::fetch('public/tips',['result'=>$result]);
    }

}