<?php

include_once APP_PATH . "./model/categoryModel.php";
class Category{
    public function checkLogin(){
        session_start();
        if(empty($_SESSION['admin_email'])){
            echo "请先登录<a href='index.php?c=login'>登录页面</a>";
            echo exit();
        }

    }

    public function categoryList(){
        $this->checkLogin();
        $categoryModel=new categoryModel();
        $categoryList=$categoryModel->getCategoryList();

        include_once APP_PATH . "./view/category_list.php";
    }

    public function categoryAdd(){
        $this->checkLogin();
        include_once APP_PATH . "./view/category_add.php";

    }
    public function categoryAddSave(){
//        $categoryId=$_POST['category_id'];
        $categoryName=$_POST['category_name'];
        $categoryDesc=$_POST['category_desc'];

//        if(empty($categoryId)){
//            echo "文章分类不能为空";
//            echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
//            echo exit();
//        }

        if(mb_strlen($categoryName)<5 || mb_strlen($categoryName) >50){
            echo "文章名称限制5-50";
            echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
            echo exit();
        }
        if(mb_strlen($categoryDesc)<10 || mb_strlen($categoryDesc) >200){
            echo "分类描述10-200";
            echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
            echo exit();
        }
        $categoryModel=new categoryModel();
        $result=$categoryModel->addCategory($categoryName,$categoryDesc);

        include_once APP_PATH . "./view/category_add_save.php";
    }

    public function categoryEdit(){
        $this->checkLogin();
        $categoryId=$_GET["category_id"];

        $categoryModel=new categoryModel();
        $category=$categoryModel->getCategoryById( $categoryId);

        include_once APP_PATH . "./view/category_edit.php";

    }
    public function categoryEditSave(){
        $this->checkLogin();
        $categoryId=$_POST['category_id'];
        $categoryName=$_POST['category_name'];
        $categoryDesc=$_POST['category_desc'];


        $categoryModel=new categoryModel();
        $result=$categoryModel->editCategorySave($categoryId,$categoryName,$categoryDesc);

        include_once APP_PATH . "./view/category_edit_save.php";
    }
    public function categoryDelete(){
        $categoryId=$_GET['category_id'];


        $categoryModel=new categoryModel();
        $result=$categoryModel->deleteCategory($categoryId);
        include_once APP_PATH . "./view/category_delete.php";
    }
}