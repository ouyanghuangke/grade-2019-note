<?php
$articleTitle=$_POST['article_title'];
$categoryId=$_POST['category_id'];
$intro=$_POST['intro'];
$content=$_POST['content'];

if(mb_strlen($articleTitle)<5 || mb_strlen($articleTitle)>50){
    echo "文章标题限制5-50";
    echo'<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    exit();
}

if(mb_strlen($intro)<10 || mb_strlen($intro)>500){
    echo "文章标题限制10-500";
    echo'<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    exit();
}
if(mb_strlen($content)<10 || mb_strlen($content)>1000){
    echo "文章内容限制10-1000";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}
if(empty($categoryId)){
    echo "需要选择文章分类";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
    echo exit();
}

include_once APP_PATH . "./model/article_add_save.php";
include_once APP_PATH . "./view/article_add_save.php";