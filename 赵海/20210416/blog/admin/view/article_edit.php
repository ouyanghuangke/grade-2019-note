<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>无标题文档</title>
    <link rel="stylesheet" href="css/index2.css" type="text/css" />
</head>

<body>
<form action="index.php?c=article_edit_save" method="post">
    <div id="top" style="width:1440px;">
        <div id="ttop" style="width:300px;float:left;"><h2>博客管理系统</h2></div>
        <div id="rtop" style="width:1140px;float:left;"><h5>管理员:<?php echo $_SESSION['admin_name']?>
                <a href="logout.php">退出登录</a></h5></div>
    </div>
    <div id="button">
        <div id="bleft">
            <ul>
                <li>
                    <a href="index.php?c=article_list" class="fenlei">分类管理</a>
                </li>
                <li>
                    <a href="index.php?c=Category&a=categoryList" class="xinwen">文章管理</a>
                </li>
                <li>
                    <a href="index.php?c=admin_list" class="guanli">管理员</a>
                </li>
            </ul>
        </div>
        <div id="bright">
            <a href="#">首页</a>&nbsp;&nbsp;&nbsp;><a href="#">文章管理</a>&nbsp;&nbsp;&nbsp;><a href="#">修改文章</a>

            <table border="1" cellspacing="0" width="1000px;" >
                                <tr>
                                    <td>文章id:</td>
                                    <td><input name="article_id" value="<?php echo $articleList['article_id']?>" type="text"/></td>
                                </tr>
                <tr>
                    <td>文章标题:</td>
                    <td><input type="text" name="article_title" value="<?php echo $articleList['article_title'];?>" /></td>
                </tr>
                <tr>
                    <td>所属分类:</td>
                    <td>
                        <select name="category_id">
                            <?php foreach ($categoryList as $row): ?>
                                <option value="<?php echo $row['category_id']?>"
                                    <?php echo $articleList['category_id']==$row['category_id'] ? 'selected="selected"':''; ?> >
                                    <?php echo $row['category_name']?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>文章简介:</td>
                    <td><textarea rows="10" cols="30" name="intro"><?php echo $articleList['intro'];?></textarea></td>
                </tr>
                <tr>
                    <td>文章内容:</td>
                    <td><textarea rows="10" cols="30" name="content"><?php echo $articleList['content'];?></textarea></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="提交" />
                        <input type="reset" value="重置" />
                    </td>
                </tr>
            </table>

        </div>
    </div>
</form>
</body>
</html>
