<?php
    include_once APP_PATH . "./view/public/header.php";
?>

    <form action="index.php?c=category_delete_qx.php"  method="get" id="plsc" >
        <div id="bright">
            <a href="#">首页</a>&nbsp;&nbsp;&nbsp;><a href="#">分类管理</a>&nbsp;&nbsp;&nbsp;><a href="#">分类列表</a>
            <input type="button" id="b" value="全选" />
            <a  class="plsc" href="javascript:void(0); ">删除选中任务</a>
            <a href="index.php?c=category&a=categoryAdd" class="zj">增加分类</a>
            <table border="1" align="center" cellspacing="0" width="1110px;" >
                <tr>
                    <th> </th>
                    <th>分类ID</th>
                    <th>分类名称</th>
                    <!--                    <th>分类</th>-->
                    <!--                    <th>简介</th>-->
                    <th>增加时间</th>
                    <th>修改时间</th>
                    <th>操作</th>
                </tr>
                <?php foreach ($categoryList as $row): ?>
                    <tr align="center">
                        <td>
                            <input name="checked[]" value="<?php echo $row['category_id'];?>" style="width:25px; vertical-align:middle; margin-top:0px;" type="checkbox" class="checkbox" />
                        </td>
                        <td><?php echo $row['category_id'];?></td>
                        <td><?php echo $row['category_name'];?></td>
                        <!--                    <td>web前端</td>-->
                        <!--                    <td>vue入门指南，适合刚接触vue的用户</td>-->
                        <td><?php echo date("Y-m-d H:i:s", $row['add_time']);?></td>
                        <td><?php echo date("Y-m-d H:i:s", $row['update_time']);?></td>
                        <td width="70px;"><a href="index.php?c=category&a=categoryEdit&category_id=<?php echo $row['category_id']?>">编辑</a>
                            <a href="index.php?c=category&a=categoryDelete&category_id=<?php echo $row['category_id']?>">删除</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

        </div>
    </form>
<script src="js/main.js"></script>
<?php include_once APP_PATH . "./view/public/foot.php";?>