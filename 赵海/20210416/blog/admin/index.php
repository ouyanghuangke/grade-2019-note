<?php
date_default_timezone_set("PRC");
define("APP_PATH",dirname(__FILE__));

$controller=$_GET['c'] ?? 'Category';

if(empty($controller)){
    echo "页面没有找到";
    exit();
}

include_once APP_PATH . "./controller/{$controller}.php";

$object=new $controller();

$action=$_GET['a'] ?? 'categoryList';
if(empty($action)){
    echo "页面没有找到";
    exit();
}
$object ->$action();

