<?php

include_once APP_PATH . './model/Model.php';
class categoryModel extends Model {


    public function getCategoryList(){

        $sql="select * from category order by category_id desc ";
        $categoryList=$this->queryAll($sql);
        return $categoryList;
    }

    public function addCategory($categoryName,$category_Desc)
    {
        $db=$this->connectDB();
//        $sql = "select * from category_name='$categoryName'";
//        $result = $db->query($sql);
//        if (!$result) {
//            echo "数据库查询错误,错误信息：" . $db->errorInfo()[2];
//            exit();
//        }
//        $category = $result->fetch(PDO::FETCH_ASSOC);
//        if ($category) {
//            echo "分类已经存在";
//            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一级</a>";
//            exit();
//        }

        $addTime = time();
        $updateTime = $addTime;

        $sql = "insert into category (category_name,category_desc,add_time,update_time) 
                    values ('$categoryName','$category_Desc','$addTime','$updateTime')";
        $result = $db->exec($sql);
        if ($result) {
            $log = [
                'action' => 'category_add',
                'content' => '分类增加成功',
                'category_name' => $categoryName,
                'category_desc' => $category_Desc,
                'time' => date("Y-m-d H:i:s", time())
            ];
            $file = fopen("log.text", "a+");
            fwrite($file, json_encode($log, JSON_UNESCAPED_UNICODE) . PHP_EOL);

        }else{
            echo "打开错误文件";
            exit();
        }

        return $result;
    }
    public function getCategoryById($categoryId){
        $sql="select * from category where category_id='$categoryId'";
        $category=$this->queryOne($sql);

        return $category;
    }

    public function editCategorySave($categoryId,$categoryName,$categoryDesc){
        $db=$this->connectDB();

        $updateTime=time();

        $sql="update category set category_name='$categoryName', category_desc='$categoryDesc',update_time='$updateTime'
        where category_id='$categoryId'";
        $result=$db->exec($sql);
        if ($result) {
            $log = [
                'action' => 'category_edit',
                'content' => '分类编辑成功',
                'category_id'=>$categoryId,
                'category_name' => $categoryName,
                'category_desc' => $categoryDesc,
                'time' => date("Y-m-d H:i:s", time())
            ];
            $file = fopen("log.text", "a+");
            fwrite($file, json_encode($log, JSON_UNESCAPED_UNICODE) . PHP_EOL);

        }else{
            echo "打开错误文件";
            exit();
        }
        return $result;
    }

    public function deleteCategory($categoryId){
        $db=$this->connectDB();

        $existSql="select * from article where category_id='$categoryId' limit 1";
        $existResult=$db->query($existSql);
        $articleResult=$existResult->fetchAll(PDO::FETCH_ASSOC);
        if($articleResult){
            echo "分类下有文章，请先删除相关文章";
            echo '<a href="javascript:void(0)" onclick="history.back()">返回上一级</a>';
            echo exit();
        }

        $sql="delete from category where category_id='$categoryId'";
        $result=$db->exec($sql);

        return $result;
    }

}