<?php
class Model{
    public function connectDB(){
        $dsn="mysql:host=127.0.0.1;dbname=blog";
        $db=new PDO($dsn,"root","123456");
        $db->exec("set names utf8mb4");
        return $db;
    }

    public function queryAll($sql){
        $db=$this->connectDB();

        $result=$db->query($sql);
        $return =$result->fetchAll(PDO::FETCH_ASSOC);
        return $return;
    }

    public function queryOne($sql){
        $db=$this->connectDB();

        $result=$db->query($sql);
        $return =$result->fetch(PDO::FETCH_ASSOC);
        return $return;
    }
}