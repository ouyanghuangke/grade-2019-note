<?php
class categoryModel{

    public function connectDB(){
        $dsn="mysql:host=127.0.0.1;dbname=blog";
        $db=new PDO($dsn,"root","123456");
        $db->exec("set names utf8mb4");
        return $db;
    }
    public function getCategoryList(){
        $db=$this->connectDB();

        $sql="select * from category order by category_id desc ";
        $result=$db->query($sql);
        $categoryList=$result->fetchAll(PDO::FETCH_ASSOC);
        return $categoryList;
    }

    public function addCategory($categoryName,$category_Desc)
    {
        $db=$this->connectDB();
//        $sql = "select * from category_name='$categoryName'";
//        $result = $db->query($sql);
//        if (!$result) {
//            echo "数据库查询错误,错误信息：" . $db->errorInfo()[2];
//            exit();
//        }
//        $category = $result->fetch(PDO::FETCH_ASSOC);
//        if ($category) {
//            echo "分类已经存在";
//            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一级</a>";
//            exit();
//        }

        $addTime = time();
        $updateTime = $addTime;

        $sql = "insert into category (category_name,category_desc,add_time,update_time) 
                    values ('$categoryName','$category_Desc','$addTime','$updateTime')";
        $result = $db->exec($sql);
        if ($result) {
            $log = [
                'action' => 'category_add',
                'content' => '分类增加',
                'category_name' => $categoryName,
                'category_desc' => $category_Desc,
                'content' => '增加分类成功',
                'time' => date("Y-m-d H:i:s", time())
            ];
            $file = fopen("log.text", "a+");
            fwrite($file, json_encode($log, JSON_UNESCAPED_UNICODE) . PHP_EOL);

        }else{
            echo "打开错误文件";
            exit();
        }

        return $result;
    }
    public function getCategoryById($categoryId){
        $db=$this->connectDB();

        $sql="select * from category where category_id='$categoryId'";
        $result=$db->query($sql);
        $category=$result->fetch(PDO::FETCH_ASSOC);

        return $category;
    }

    public function editCategorySave($categoryName,$categoryId,$categoryDesc){
        $db=$this->connectDB();

        $updateTime=time();

        $sql="update category set category_name='$categoryName', category_desc='$categoryDesc',update_time='$updateTime'
        where category_id='$categoryId'";
        $result=$db->exec($sql);
        if ($result) {
            $log = [
                'action' => 'category_edit',
                'content' => '分类编辑',
                'category_id'=>$categoryId,
                'category_name' => $categoryName,
                'category_desc' => $categoryDesc,
                'time' => date("Y-m-d H:i:s", time())
            ];
            $file = fopen("log.text", "a+");
            fwrite($file, json_encode($log, JSON_UNESCAPED_UNICODE) . PHP_EOL);

        }else{
            echo "打开错误文件";
            exit();
        }
        return $result;
    }

}