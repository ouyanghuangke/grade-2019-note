<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Category
{
    public function index()
    {
        $categoryList=CategoryModel::select();

        View::assign('categoryList',$categoryList);
        return View::fetch();
    }
    public function add(){

        return View::fetch();

    }
    public function addSave(){
        $params=Request::param();
        $validate=Validate::rule([
            'category_name|分类名称'=>'require|min:2|max:45',
            'category_desc|分类描述'=>'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            echo $validate->getError();
            exit();
        }
        $params['add_time']=time();
        $params['update_time']=time();
        $result=CategoryModel::create($params);
        if($result){
            echo "操作成功。<a href='index.php?s=admin/category/index'>返回列表页面</a>";
        }else{
            echo "操作失败。<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
        }
    }
    public function edit(){
        $categoryId=Request::param(['category_id']);
        $validate=Validate::rule([
            'category_id'=>'require',
        ]);
        if (!$validate->check(['category_id'=>$categoryId])) {
            echo $validate->getError();
            exit();
        }
        $category=CategoryModel::find($categoryId);
        return View::fetch('',['category'=>$category]);

    }
    public function editSave(){
        $params=Request::param();
        $validate=Validate::rule([
            'category_id'=>'require',
            'category_name'=>'require|min:2|max:45',
            'category_desc'=>'require|min:10|max:255'
        ]);
        if (!$validate->check($params)) {
            echo $validate->getError();
            exit();
        }
        $category=CategoryModel::find($params['category_id']);
        if(!$category){
            echo '分类存在';
            exit();
        }
        $category['category_name']=$params['category_name'];
        $category['category_desc']=$params['category_desc'];
        $result=$category->save();
        if($result){
            echo "操作成功。<a href='index.php?s=admin/category/index'>返回列表页面</a>";
        }else{
            echo "操作失败。<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
        }
    }
    public function delete(){
        $category_id=Request::param('category_id');
        $validate=Validate::rule([
            'category_id'=>'require',
        ]);
        if(!$validate->check(['category_id'=>$category_id])){
            echo $validate->getError();
            exit();
        }


        $result=CategoryModel::destroy($category_id);if($result){
            echo "操作成功。<a href='index.php?s=admin/category/index'>返回列表页面</a>";
        }else{
            echo "操作失败。<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
        }
    }
}

