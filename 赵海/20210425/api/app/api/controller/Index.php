<?php
declare (strict_types = 1);

namespace app\api\controller;

use think\facade\Request;
use think\facade\Validate;

class Index
{
    public function index()
    {
        return '您好！这是一个[api]示例应用';
    }

    public function zfc()
    {
        $str = Request::param('str');
        $validate = Validate::rule([
            'str' => 'require',
        ]);
        if (!$validate->check(['str' => $str])){
            $data=[
                'status'=>1,
                'message'=>$validate->getError(),
                'data'=>[]
            ];
            return json($data);
        }

        $zfc=mb_strlen($str);
        $data=[
            'status'=>0,
            'message'=>'',
            'data'=>[
                'length'=>$zfc,
            ]
        ];
        return json($data);

    }
    public function num()
    {
        $num = Request::param('num');
        $validate = Validate::rule([
            'num' => 'require|between:1,' . PHP_INT_MAX,
        ]);
        $validate->check(['num' => $num]);

    }

}
