<?php

$words=explode(" ",$_POST['word']);

$contentList=[];
foreach ($words as $row){
    if (empty($contentList[$row])){
        $contentList[$row]=0;
    }
    $contentList[$row]++;
}

$data=[
    'status'=>0,
    'message'=>'success',
    'data'=>[
        'contentList'=>$contentList
    ]
];

header("Content-Type","application/json");
echo json_encode($data,JSON_UNESCAPED_UNICODE);
