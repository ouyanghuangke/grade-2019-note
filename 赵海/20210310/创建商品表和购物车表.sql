CREATE TABLE `item` (
  `item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(100) NOT NULL COMMENT '商品名称',
  `item_price` decimal(10,2) NOT NULL COMMENT '商品价格',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='商品表';

CREATE TABLE `cart` (
  `cart_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL COMMENT '用户id',
  `item_id` int(11) unsigned NOT NULL COMMENT '商品id',
  `item_num` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '商品数量',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '加入时间',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;